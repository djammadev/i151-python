import itertools

import math
import numpy as np

TEN = 5
NINE = 4
SEVEN = 3
KING = 2
QUEEN = 1
JOKER = 0
AS = 6
EIGHT = 7

DIAMOND = 0
CLUB = 1
HEART = 2
SPADE = 3

CARD_LENGTH = 32
CARD_COLOR_LENGTH = 4

SCORE_LIMIT = 151

WEIGHTS = np.array([2, 2, 2, 2,  # Joker
                    3, 3, 3, 3,  # Queen
                    4, 4, 4, 4,  # King
                    7, 7, 7, 7,
                    9, 9, 9, 9,
                    10, 10, 10, 10,
                    11, 11, 11, 11,  # As
                    32, 32, 32, 32])

INPUT_DIM = 64
ACTION_MAX_DIM = 74
OUTPUT_DIM = 598
ASKED_INDEX = 594  # 594-597
PASS_INDEX = 593
FOUR_EIGHT_INDEX = 592


def shuffle(array):
    for i in range(array.size - 1, 0, -1):
        j = int(np.floor(np.random.random() * (i + 1)))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    return array


def riffleShuffle(array, n):
    for _ in range(0, n):
        array = shuffle(array)
    return array


def cardNumber(card):
    if card == -1:
        return -1
    return np.floor(card / 4)


def cardColor(card):
    return card % 4


def isSameNumber(hand, number):
    handSize = len(hand)
    if handSize < 1:
        return False
    for c in range(0, handSize):
        if cardNumber(hand[c]) != number:
            return False
    return True


# Tested
def isValid(hand, topCard=-1, asked=-1, playerCount=2):
    if not isProbableHand(hand):
        return False
    handSize = hand.size
    oneCard = handSize == 1
    if topCard == -1:
        return oneCard or isSameNumber(hand, EIGHT)
    if isSameNumber(hand, EIGHT):
        return True
    if cardNumber(topCard) == EIGHT:
        return cardColor(hand[0]) == asked and oneCard
    if handSize > 1 or cardNumber(topCard) == AS:
        return isSameNumber(hand, cardNumber(topCard)) or isSameNumber(hand, EIGHT)
    return cardColor(topCard) == cardColor(hand[0]) or cardNumber(topCard) == cardNumber(hand[0]) or isSameNumber(hand,
                                                                                                                  EIGHT)


def isProbableHand(hand):
    handSize = hand.size
    oneCard = handSize == 1
    if oneCard:
        return True
    valid = np.zeros(CARD_LENGTH)
    for c in hand:
        if valid[c] == 1:
            return False
        valid[c] = 1
    return True


def numberCount(hand, number):
    count = 0
    for c in range(0, hand.size):
        if cardNumber(hand[c]) == number:
            count += 1
    return count


def eightActions(hand):
    eightCounts = []
    actions = np.array([])
    handSize = hand.size
    for i in range(0, handSize):
        if cardNumber(hand[i]) == EIGHT:
            eightCounts.append(hand[i])
    if len(eightCounts) >= 1:
        for i in range(len(eightCounts) - 1, -1, -1):
            act = np.zeros(CARD_LENGTH)
            for c in range(0, len(eightCounts[i:])):
                act[eightCounts[i:][c]] = c + 1
            actions = np.append(actions, act)
    return actions


def colorActions(hand, color):
    actions = np.array([])
    handSize = hand.size
    for i in range(0, handSize):
        if (cardColor(hand[i]) == color) and (cardNumber(hand[i]) != EIGHT):
            act = np.zeros(CARD_LENGTH)
            act[hand[i]] = 1
            actions = np.append(actions, act)
    return actions


def numberActions(hand, number):
    actions = np.array([])
    handSize = hand.size
    sameNumbers = []
    for c in range(0, handSize):
        if cardNumber(hand[c]) == number:
            sameNumbers.append(hand[c])
    count = len(sameNumbers)
    values = np.zeros(OUTPUT_DIM)
    for perm in list(itertools.permutations(sameNumbers)):
        for i in range(count - 1, -1, -1):
            act = np.zeros(CARD_LENGTH)
            for j in range(0, len(perm[i:])):
                act[perm[i:][j]] = j + 1
            index = getIndex(perm[i:])
            if values[index] == 0:
                values[index] = 1
                actions = np.append(actions, act)
    return actions


def availableActions(hand, topCard=-1, asked=-1):
    actions = np.array([])
    hand.sort()
    handSize = hand.size
    if topCard == -1:  # Start of Game
        for i in range(0, handSize):
            act = np.zeros(CARD_LENGTH)
            if cardNumber(hand[i]) != EIGHT:
                act[hand[i]] = 1
                actions = np.append(actions, act)
    elif cardNumber(topCard) == EIGHT:
        color_actions = colorActions(hand, asked)
        if color_actions.size > 0:
            actions = np.append(actions, color_actions)
    else:
        number_actions = numberActions(hand, cardNumber(topCard))
        if number_actions.size > 0:
            actions = np.append(actions, number_actions)
        if cardNumber(topCard) != AS:
            color_actions = colorActions(hand, cardColor(topCard))
            if color_actions.size > 0:
                actions = np.append(actions, color_actions)
    if cardNumber(topCard) != EIGHT or asked != -1:  # Should ask
        eight_actions = eightActions(hand)
        if eight_actions.size > 0:
            actions = np.append(actions, eight_actions)
    return actions


def getActionVector(hand, topCard=-1, asked=-1):
    actionVector = np.zeros((OUTPUT_DIM,))
    actions = availableActions(hand, topCard, asked)
    if actions.size == 0 and cardNumber(topCard) == EIGHT and asked == -1:
        actionVector[[ASKED_INDEX + i for i in range(CARD_COLOR_LENGTH)]] = 1
    indexes = np.array([])
    for action in np.reshape(actions, (-1, CARD_LENGTH)):
        myAction = np.array([])
        for i in range(1, 5):
            myAction = np.append(myAction, np.nonzero(action == i)).astype(int)
        indexes = np.append(indexes, getIndex(myAction))
    indexes = indexes.astype(int)
    # print(indexes)
    actionVector[indexes] = 1
    if topCard != -1 and (cardNumber(
            topCard) != EIGHT or asked != -1):  # Can't pass if it is the first play or if I have to ask a color.
        actionVector[PASS_INDEX] = 1  # Pass action
    return actionVector


def getScore(value, prevScore=SCORE_LIMIT):
    return prevScore - np.sum(WEIGHTS * value, axis=1)


def convertAvailableActions(actions):
    result = np.zeros((len(actions), CARD_LENGTH))
    result[np.nonzero(actions >= 1)] = 1
    return result


class Player:

    def __init__(self, game, name):
        self.game = game
        self.name = name
        self.score = SCORE_LIMIT
        self.rewards = 0
        self.totalRewards = 0
        self.victory = 0
        self.asked = -1
        self.cumulVictory = 0
        self.fullVictory = 0
        self.prevHand = []

    def reset(self, hand):
        self.prevHand = np.array([])
        self.hand = hand
        self.handVector = self.handToVector()
        self.actions = []
        self.playedCards = []
        self.rewards = 0
        self.cumulVictory = 0

    def availableMoves(self):
        actions = self.getActions(self.game.topCard, self.game.asked)
        availActions = np.zeros(OUTPUT_DIM)
        for act in actions:
            availActions[encodeCard(act)] = 1
        return availActions

    def getState(self):
        currStates = np.zeros(INPUT_DIM)
        if self.game.table.size != 0:
            currStates[CARD_LENGTH + self.game.table] = 1
        currStates[self.hand] = 1
        return currStates

    def autoPlay(self):
        if self.game.asked == -1 and cardNumber(self.game.topCard) == EIGHT:
            asked = self.ask()
            return ASKED_INDEX + asked
        else:
            cards = self.play()
            return getIndex(cards)

    def play(self):
        X = self.handToVector()
        actions = self.getActions(self.game.topCard, self.game.asked)
        if len(actions) == 0:
            actions = np.append(actions, np.zeros(CARD_LENGTH))
        actions = np.reshape(actions, (-1, CARD_LENGTH))
        self.actions = actions
        self.handVector = X
        A = convertAvailableActions(actions)
        S = getScore(X - A, self.score)
        index = np.argmax(S)
        played = actions[index]
        self.played = played
        result = np.array([])
        for i in range(1, 5):
            result = np.append(result, np.nonzero(played == i))
        self.playedCards = result
        self.prevHand = self.hand.copy()
        return result.astype(int)

    def ask(self):
        colorInTable = np.zeros(4)
        myColors = np.zeros(4)
        tableVector = np.zeros(CARD_LENGTH)
        tableVector[self.game.table] = 1
        for i in range(8):
            for j in range(4):
                colorInTable[j] += tableVector[i * 4 + j]
                myColors[j] += self.handVector[i * 4 + j]
        color = np.argmax(colorInTable + myColors)
        self.asked = color
        return color

    def state(self):
        states = {}
        game = self.game
        states["actions"] = self.actions
        states["score"] = self.score
        states["hand"] = self.handVector
        states["playedCards"] = self.playedCards
        return states

    def updateScore(self):
        self.score = self.getHandScore()
        if self.cumulVictory == 3:
            self.score += 10
            self.rewards += 10
            self.cumulVictory = 0

    def getActions(self, topCard, asked):
        return availableActions(self.hand, topCard, asked)

    def handToVector(self):
        result = np.zeros(CARD_LENGTH)
        result[self.hand] = 1
        return result

    def getHandScore(self):
        X = self.handToVector()
        return self.score - np.sum(WEIGHTS * X)

    def getRewards(self):
        X = self.handToVector()
        return np.sum(WEIGHTS * X)

    def isOut(self):
        return self.score <= 0

    def __repr__(self):
        return "<%s, Rewards: %d, Total Rewards: %d, Victory: %d, Full: %d>" % (
            self.name, self.rewards, self.totalRewards, self.victory, self.fullVictory)


def encode(number, colors=None):
    if colors is None:
        colors = np.array([])
    dim = len(colors)
    if dim == 0:
        return PASS_INDEX  # Pass
    if dim == 4:
        return FOUR_EIGHT_INDEX  # 4 x 7 only four 7 is allowed
    scale = 0
    if dim > 1:
        scale = math.pow(4, dim - 1)
    base = np.asarray([math.pow(4, dim - i - 1) for i in range(dim)])
    return int(np.sum(base * colors) + scale + number * ACTION_MAX_DIM)


def decode(code):
    code = np.squeeze(code)
    if code >= ASKED_INDEX:
        return np.array([]), code - ASKED_INDEX
    elif code == FOUR_EIGHT_INDEX:
        return np.array([EIGHT * 4 + i for i in range(4)]), -1
    elif code == PASS_INDEX:
        return np.array([]), -1
    else:
        dim = 0
        number = math.floor(code / ACTION_MAX_DIM)
        value = code % ACTION_MAX_DIM
        if value < 4:
            dim = 1
        elif value < 20:
            dim = 2
        elif value < 74:
            dim = 3
        scale = 0
        if dim > 1:
            scale = math.pow(4, dim - 1)
        colors = value - scale

        result = []
        while colors > 0:
            result.append(colors % 4)
            colors = math.floor(colors / 4)
        if len(result) < dim:
            for i in range(dim - len(result)):
                result.append(0)
        return (4 * number) + np.flip(result).astype(int), -1


def encodeCard(cards):
    result = np.array([])
    for i in range(1, 5):
        result = np.append(result, np.nonzero(cards == i)[0])
    if len(result) > 0:
        number = cardNumber(result[0])
        colors = np.array([c % 4 for c in result])
        result = encode(number, colors)
    else:
        result = np.append(result, PASS_INDEX)
    return result.astype(int)


# cards should be a valid hand
def getIndex(cards):
    if len(cards) == 0:
        return PASS_INDEX
    number = cardNumber(cards[0])
    colors = np.array([c % 4 for c in cards])
    return encode(number, colors)


def getRewards(hand):
    return np.sum(WEIGHTS[hand])


def encodeObs(obs):
    size = len(obs)
    result = 0
    for i in range(size):
        result = 2 * result + obs[i]
    return result
