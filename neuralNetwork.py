import matplotlib.pyplot as plt
import os
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers


def plot_graphs(history, string):
    print(history)
    plt.plot(history.history[string])
    plt.plot(history.history['val_' + string])
    plt.xlabel("Epochs")
    plt.ylabel(string)
    plt.legend([string, 'val_' + string])
    plt.show()


def build_model():
    model = keras.Sequential([
        layers.Dense(598, activation='relu', input_shape=(68,)),
        layers.Dense(512, activation='relu'),
        layers.Dense(256, activation='relu'),
        layers.Dense(598)
    ])

    optimizer = tf.keras.optimizers.RMSprop(0.001)

    model.compile(loss='mse',
                  optimizer=optimizer,
                  metrics=['mae', 'accuracy'])
    return model

def load_model(checkpoint_path):
    model = build_model()
    model.load_weights(checkpoint_path)
    return model

EPOCHS = 100


def train(inputs, epochs=EPOCHS,checkpoint_path = "gameModel.ckpt"):
    model = build_model()
    checkpoint_dir = os.path.dirname(checkpoint_path)

    # Create a callback that saves the model's weights
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only=True,
                                                     verbose=1)
    history = model.fit(
        inputs["X"], inputs["Y"],
        epochs=epochs, validation_split=0.1, verbose=1,
        callbacks=[cp_callback])

    return model, history
