from multiprocessing import Process, Pipe

import joblib
import numpy as np
import tensorflow as tf

import gameLogic
from PPONetwork import PPONetwork, PPOModel


def sf01(arr):
    """
    swap and then flatten axes 0 and 1
    """
    s = arr.shape
    return arr.swapaxes(0, 1).reshape(s[0] * s[1], *s[2:])


class Game:
    def __init__(self, count=2):
        self.nbPlayer = count
        self.endGame = False
        self.training = True
        self.players = {}
        for p in range(0, self.nbPlayer):
            self.players[p] = gameLogic.Player(self, name="Player %s" % p)
        self.reset()

    def init(self):
        self.reset()
        for p in range(0, self.nbPlayer):
            self.players[p].score = gameLogic.SCORE_LIMIT

    def reset(self):
        self.currentPlayer = 0
        self.gameOver = False
        self.goCounter = 0
        self.deck = np.arange(0, gameLogic.CARD_LENGTH, 1)
        self.table = np.array([])
        self.topCard = -1
        self.asked = -1
        self.takeTwo = False
        self.hasTook = False
        self.failed = 0
        self.rewards = np.zeros((self.nbPlayer,))
        gameLogic.riffleShuffle(self.deck, 2)
        for p in range(0, self.nbPlayer):
            hand = self.deck[0:8]
            self.players[p].reset(hand)
            self.deck = self.deck[8:]

    def take(self, nbCards=1):
        cards = np.array([])
        if nbCards > self.deck.size and self.deck.size > 0:
            cards = np.append(cards, self.deck)
            self.deck = np.delete(self.deck, range(0, self.deck.size))  # Empty
            remain = nbCards - self.table.size
            cards = np.append(cards, self.table[0:remain])
            self.table = self.table[remain:]
        elif self.deck.size == 0:
            cards = np.append(cards, self.table[0:nbCards])
            self.table = self.table[nbCards:]
        else:
            cards = np.append(cards, self.deck[0:nbCards])
            self.deck = self.deck[nbCards:]
        return cards

    def next(self):
        self.currentPlayer = (self.currentPlayer + 1) % self.nbPlayer

    def updateScore(self):
        totalRewards = 0
        for p in range(self.nbPlayer):
            self.players[p].updateScore()
            if self.players[p].hand.size == 0:
                winner = p
                self.players[p].cumulVictory += 1
            else:
                self.players[p].rewards -= self.players[p].hand.size
                self.rewards[p] = -self.players[p].hand.size
                totalRewards += self.players[p].hand.size
        self.players[winner].rewards += totalRewards
        self.rewards[winner] = totalRewards

    def resetAsk(self):
        self.asked = -1

    def step(self, action):
        self.goCounter += 1
        current = self.currentPlayer
        # if current == 0:
        #    print("action = %d" % action)
        player = self.players[current]
        playedCards, ask = gameLogic.decode(action)
        if ask != -1 and len(playedCards) == 0:  # Current Player ask a color
            self.asked = ask
            self.next()
        else:
            if len(playedCards) == 0:  # Player want to pass or draw
                if not self.hasTook:
                    nbCards = 1
                    if self.takeTwo:
                        nbCards += 1
                        self.takeTwo = False
                    taken = self.take(nbCards)
                    player.hand = np.append(player.hand, taken).astype(int)
                    self.hasTook = True
                    if self.takeTwo:
                        self.next()
                else:  # Player already took so pass the round.
                    self.next()
                    self.hasTook = False
            elif gameLogic.isValid(playedCards, self.topCard, self.asked):
                self.table = np.append(self.table, playedCards).astype(int)
                self.resetAsk()
                self.topCard = playedCards[-1]  # Last Card
                number = gameLogic.cardNumber(self.topCard)
                self.takeTwo = number == gameLogic.AS
                pVect = np.zeros(gameLogic.CARD_LENGTH)
                pVect[playedCards] = 1
                hVect = player.handToVector()
                hValue = hVect - pVect
                hand = np.array([])
                hand = np.append(hand, np.nonzero(hValue > 0))
                player.hand = hand.astype(int)
                if player.hand.size == 0:
                    if number != gameLogic.QUEEN:
                        self.updateScore()
                        player.victory += 1
                        self.gameOver = True
                    else:
                        self.next()
                        self.next()
                elif number == gameLogic.QUEEN or number != gameLogic.EIGHT:
                    self.next()
                    if number == gameLogic.QUEEN:
                        self.next()
            else:
                print("Wrong : Player: %d, PlayedCards: %s" % (current, playedCards))
                self.failed += 1
        # print("Step: %d, Failed: %d, GameOver: %d, hasTook: %d, CurrentPlayer: %d, PlayedCards: %s %s"% (self.goCounter, self.failed, self.gameOver, self.hasTook, self.currentPlayer, playedCards, self.players))

        if self.gameOver == False:
            reward = 0
            done = False
            info = None
        else:
            reward = self.rewards
            done = False
            info = {}
            info['numTurns'] = self.goCounter
            finalReward = np.zeros(self.nbPlayer)
            terminated = False
            for p in self.players:
                if self.players[p].isOut():
                    terminated = True
                    finalReward[p] = self.players[p].score
                else:
                    finalReward[p] = self.players[p].score
            if terminated:
                for p in self.players:
                    self.players[p].rewards += finalReward[p]
                    self.players[p].score += finalReward[p]
                    reward[p] += finalReward[p]
                done = True
                self.endGame = True
            info['rewards'] = reward
            if self.training:
                if terminated:
                    self.init()
                else:
                    self.reset()
        return reward, done, info

    def getCurrentState(self):
        player = self.players[self.currentPlayer]
        currStates = player.getState()
        availableActions = gameLogic.getActionVector(player.hand, self.topCard, self.asked)
        availableActions[np.nonzero(availableActions == 1)] = 100
        if availableActions[gameLogic.PASS_INDEX] > 0:  # Do not pass if i can play (not sure)
            availableActions[gameLogic.PASS_INDEX] = 10
        actions = availableActions.reshape(1, gameLogic.OUTPUT_DIM)
        # if self.currentPlayer == 0:
        #    print("top: %d, actions = %s" % (self.topCard, np.nonzero(actions > 0)[1]))
        availableActions[np.nonzero(availableActions == 0)] = -10000
        return self.currentPlayer, currStates.reshape(1, gameLogic.INPUT_DIM), actions


# now create a vectorized environment
def worker(remote, parent_remote):
    parent_remote.close()
    game = Game()
    while True:
        cmd, data = remote.recv()
        if cmd == 'step':
            reward, done, info = game.step(data)
            remote.send((reward, done, info))
        elif cmd == 'reset':
            game.reset()
            pGo, cState, availAcs = game.getCurrentState()
            remote.send((pGo, cState))
        elif cmd == 'getCurrState':
            pGo, cState, availAcs = game.getCurrentState()
            remote.send((pGo, cState, availAcs))
        elif cmd == 'close':
            remote.close()
            break
        else:
            print("Invalid command sent by remote")
            break


class vectorizedBig2Games(object):
    def __init__(self, nGames):

        self.waiting = False
        self.closed = False
        self.remotes, self.work_remotes = zip(*[Pipe() for _ in range(nGames)])
        self.ps = [Process(target=worker, args=(work_remote, remote)) for (work_remote, remote) in
                   zip(self.work_remotes, self.remotes)]

        for p in self.ps:
            p.daemon = True
            p.start()
        for remote in self.work_remotes:
            remote.close()

    def step_async(self, actions):
        for remote, action in zip(self.remotes, actions):
            remote.send(('step', action))
        self.waiting = True

    def step_wait(self):
        results = [remote.recv() for remote in self.remotes]
        self.waiting = False
        rewards, dones, infos = zip(*results)
        return rewards, dones, infos

    def step(self, actions):
        self.step_async(actions)
        return self.step_wait()

    def currStates_async(self):
        for remote in self.remotes:
            remote.send(('getCurrState', None))
        self.waiting = True

    def currStates_wait(self):
        results = [remote.recv() for remote in self.remotes]
        self.waiting = False
        pGos, currStates, currAvailAcs = zip(*results)
        return np.stack(pGos), np.stack(currStates), np.stack(currAvailAcs)

    def getCurrStates(self):
        self.currStates_async()
        return self.currStates_wait()

    def close(self):
        if self.closed:
            return
        if self.waiting:
            for remote in self.remotes:
                remote.recv()
        for remote in self.remotes:
            remote.send(('close', None))
        for p in self.ps:
            p.join()
        self.closed = True


class gameSimulation:
    def __init__(self, sess, *, count=2, inpDim=64, nGames=8, nSteps=20, nMiniBatches=4,
                 nOptEpochs=5, lam=0.95,
                 gamma=0.995, ent_coef=0.01, vf_coef=0.5, max_grad_norm=0.5, minLearningRate=0.000001, learningRate,
                 clipRange, saveEvery=100, fromParams=None):
        self.nbPlayer = count
        self.currentPlayer = 0
        self.endGame = False
        self.gameOver = False
        self.goCounter = 0
        self.rewards = np.zeros((count,))
        self.states = []
        self.inputs = []
        self.training = True
        self.trainingNetwork = PPONetwork(sess, inpDim, gameLogic.OUTPUT_DIM, "trainNet")
        if fromParams is not None:
            params = joblib.load(fromParams)
            self.trainingNetwork.loadParams(params)
        self.trainingModel = PPOModel(sess, self.trainingNetwork, inpDim, gameLogic.OUTPUT_DIM, ent_coef, vf_coef,
                                      max_grad_norm)
        self.players = {}
        for p in range(0, self.nbPlayer):
            self.players[p] = gameLogic.Player(self, "Player %s" % p)

        tf.global_variables_initializer().run(session=sess)

        # environment
        self.vectorizedGame = vectorizedBig2Games(nGames)

        # params
        self.nGames = nGames
        self.inpDim = inpDim
        self.nSteps = nSteps
        self.nMiniBatches = nMiniBatches
        self.nOptEpochs = nOptEpochs
        self.lam = lam
        self.gamma = gamma
        self.learningRate = learningRate
        self.minLearningRate = minLearningRate
        self.clipRange = clipRange
        self.saveEvery = saveEvery

        self.rewardNormalization = 10.0  # divide rewards by this number (so reward ranges from -1.0 to 3.0)

        # final 4 observations need to be carried over (for value estimation and propagating rewards back)
        self.prevObs = []
        self.prevGos = []
        self.prevAvailAcs = []
        self.prevRewards = []
        self.prevActions = []
        self.prevValues = []
        self.prevDones = []
        self.prevNeglogpacs = []

        # episode/training information
        self.totTrainingSteps = 0
        self.epInfos = []
        self.gamesDone = 0
        self.losses = []
        self.minLosses = []
        self.failed = 0
        self.reset()

    def take(self, nbCards=1):
        cards = np.array([])
        if nbCards > self.deck.size and self.deck.size > 0:
            cards = np.append(cards, self.deck)
            self.deck = np.delete(self.deck, range(0, self.deck.size))  # Empty
            remain = nbCards - self.table.size
            cards = np.append(cards, self.table[0:remain])
            self.table = self.table[remain:]
        elif self.deck.size == 0:
            cards = np.append(cards, self.table[0:nbCards])
            self.table = self.table[nbCards:]
        else:
            cards = np.append(cards, self.deck[0:nbCards])
            self.deck = self.deck[nbCards:]
        return cards

    def next(self):
        self.currentPlayer = (self.currentPlayer + 1) % self.nbPlayer

    def updateScore(self):
        totalRewards = 0
        for p in range(self.nbPlayer):
            self.players[p].updateScore()
            if self.players[p].hand.size == 0:
                winner = p
                self.players[p].cumulVictory += 1
            else:
                self.players[p].rewards -= self.players[p].hand.size
                totalRewards += self.players[p].hand.size
            if not self.training:
                self.endGame = self.endGame or self.players[p].isOut()
        self.players[winner].rewards += totalRewards

    def updateStep(self):
        endGame = False
        for p in range(self.nbPlayer):
            if self.players[p].isOut():
                endGame = True
                break
        self.endGame = endGame
        playersStates = {}
        for p in range(self.nbPlayer):
            playersStates[p] = self.players[p].state()
        state = {}
        state["playersStates"] = playersStates
        state["topCard"] = self.topCard
        state["asked"] = self.asked
        state["hasTook"] = self.hasTook
        state["table"] = self.table
        state["deck"] = self.deck
        return state

    def restart(self):
        for p in range(0, self.nbPlayer):
            self.players[p].score = gameLogic.SCORE_LIMIT
        self.run()

    def reset(self):
        self.deck = np.arange(0, gameLogic.CARD_LENGTH, 1)
        self.table = np.array([])
        self.topCard = -1
        self.asked = -1
        self.takeTwo = False
        self.hasTook = False
        # self.endGame = False
        self.gameOver = False
        self.goCounter = 0
        self.rewards = np.zeros((self.nbPlayer,))
        gameLogic.riffleShuffle(self.deck, 2)
        for p in range(0, self.nbPlayer):
            hand = self.deck[0:8]
            self.players[p].reset(hand)
            self.deck = self.deck[8:]

    def addInput(self, player):
        T = np.zeros(gameLogic.CARD_LENGTH + 4)
        if self.table.size > 0:
            T[self.table] = 1
        if self.asked >= 0:
            T[gameLogic.CARD_LENGTH + self.asked] = 1
        X = np.array([])
        X = np.append(X, player.handToVector())  # Add Hand
        X = np.append(X, T)  # Add Table
        Y = np.zeros(gameLogic.OUTPUT_DIM)
        Y[gameLogic.encodeCard(player.played)] = 1
        if player.asked >= 0:
            Y[gameLogic.ASKED_INDEX + player.asked] = 1
        self.inputs.append([X, Y])

    def resetAsk(self):
        self.asked = -1
        for p in range(self.nbPlayer):
            self.players[p].asked = -1

    def step(self, action):
        self.goCounter += 1
        player = self.players[self.currentPlayer]
        playedCards, ask = gameLogic.decode(action)
        if ask != -1:  # Current Player ask a color
            self.asked = ask
            self.next()
        else:
            if len(playedCards) == 0:  # Player want to pass or draw
                if not self.hasTook:
                    nbCards = 1
                    if self.takeTwo:
                        nbCards += 1
                        self.takeTwo = False
                    taken = self.take(nbCards)
                    player.hand = np.append(player.hand, taken).astype(int)
                    self.hasTook = True
                    if self.takeTwo:
                        self.next()
                else:  # Player already took so pass the round.
                    self.next()
                    self.hasTook = False
            elif gameLogic.isValid(playedCards, self.topCard, self.asked):
                self.table = np.append(self.table, playedCards).astype(int)
                self.resetAsk()
                self.topCard = playedCards[-1]  # Last Card
                number = gameLogic.cardNumber(self.topCard)
                self.takeTwo = number == gameLogic.AS
                pVect = np.zeros(gameLogic.CARD_LENGTH)
                pVect[playedCards] = 1
                hVect = player.handToVector()
                hValue = hVect - pVect
                hand = np.array([])
                hand = np.append(hand, np.nonzero(hValue > 0))
                player.hand = hand.astype(int)
                if player.hand.size == 0:
                    if number != gameLogic.QUEEN:
                        self.updateScore()
                        player.victory += 1
                        self.gameOver = True
                    else:
                        self.next()
                        self.next()
                elif number == gameLogic.QUEEN or number != gameLogic.EIGHT:
                    self.next()
                    if number == gameLogic.QUEEN:
                        self.next()
            else:
                print("Wrong : Player: %d, PlayedCards: %s" % (self.currentPlayer, playedCards))
                self.failed += 1
        # print("Step: %d, Failed: %d, GameOver: %d, hasTook: %d, CurrentPlayer: %d, PlayedCards: %s %s"% (self.goCounter, self.failed, self.gameOver, self.hasTook, self.currentPlayer, playedCards, self.players))

        if self.gameOver == False:
            reward = 0
            done = False
            info = None
        else:
            reward = np.zeros((self.nbPlayer,))
            done = True
            info = {}
            info['numTurns'] = self.goCounter
            winner_reward = 0
            self.reset()
            winner = -1
            for p in self.players:
                if self.players[p].hand.size > 0:
                    p_reward = self.players[p].getRewards()
                    winner_reward += p_reward
                    reward[p] = -p_reward
                else:
                    winner = p
            if winner != -1:
                reward[winner] = winner_reward
            self.rewards = reward
            info['rewards'] = self.rewards
        return [reward], [done], [info]

    def getCurrStates(self):
        player = self.players[self.currentPlayer]
        currStates = player.getState()
        availableActions = gameLogic.getActionVector(player.hand, self.topCard, self.asked)
        availableActions[np.nonzero(availableActions == 1)] = 100
        if availableActions[gameLogic.PASS_INDEX] > 0:  # Do not pass if i can play
            availableActions[gameLogic.PASS_INDEX] = 10
        availableActions[np.nonzero(availableActions == 0)] = -10000
        actions = availableActions.reshape(1, gameLogic.OUTPUT_DIM)
        print("actions = %s" % np.nonzero(actions > 0)[0])
        return [self.currentPlayer], currStates.reshape(1, gameLogic.INPUT_DIM), actions

    def run(self):
        # run vectorized games for nSteps and generate mini batch to train on.
        mb_obs, mb_pGos, mb_actions, mb_values, mb_neglogpacs, mb_rewards, mb_dones, mb_availAcs = [], [], [], [], [], [], [], []
        for i in range(len(self.prevObs)):
            mb_obs.append(self.prevObs[i])
            mb_pGos.append(self.prevGos[i])
            mb_actions.append(self.prevActions[i])
            mb_values.append(self.prevValues[i])
            mb_neglogpacs.append(self.prevNeglogpacs[i])
            mb_rewards.append(self.prevRewards[i])
            mb_dones.append(self.prevDones[i])
            mb_availAcs.append(self.prevAvailAcs[i])
        if len(self.prevObs) == self.nbPlayer:
            endLength = self.nSteps
        else:
            endLength = self.nSteps - self.nbPlayer
        for _ in range(self.nSteps):
            currGos, currStates, currAvailAcs = self.vectorizedGame.getCurrStates()
            currStates = np.squeeze(currStates)
            currAvailAcs = np.squeeze(currAvailAcs)
            currGos = np.squeeze(currGos)
            actions, values, neglogpacs = self.trainingNetwork.step(currStates, currAvailAcs)
            rewards, dones, infos = self.vectorizedGame.step(actions)
            mb_obs.append(currStates.copy())
            mb_pGos.append(currGos)
            mb_availAcs.append(currAvailAcs.copy())
            mb_actions.append(actions)
            mb_values.append(values)
            mb_neglogpacs.append(neglogpacs)
            mb_dones.append(list(dones))
            # now back assign rewards if state is terminal
            toAppendRewards = np.zeros((self.nGames,))
            mb_rewards.append(toAppendRewards)
            for i in range(self.nGames):
                if dones[i] == True:
                    reward = rewards[i]
                    for p in range(1, self.nbPlayer + 1):
                        mb_rewards[-p][i] = reward[mb_pGos[-p][i] - 1] / self.rewardNormalization
                    for p in range(2, self.nbPlayer + 1):
                        mb_dones[-p][i] = True
                    self.epInfos.append(infos[i])
                    self.gamesDone += 1
                    print("Game %d finished. Lasted %d turns" % (self.gamesDone, infos[i]['numTurns']))
        self.prevObs = mb_obs[endLength:]
        self.prevGos = mb_pGos[endLength:]
        self.prevRewards = mb_rewards[endLength:]
        self.prevActions = mb_actions[endLength:]
        self.prevValues = mb_values[endLength:]
        self.prevDones = mb_dones[endLength:]
        self.prevNeglogpacs = mb_neglogpacs[endLength:]
        self.prevAvailAcs = mb_availAcs[endLength:]
        mb_obs = np.asarray(mb_obs, dtype=np.float32)[:endLength]
        mb_availAcs = np.asarray(mb_availAcs, dtype=np.float32)[:endLength]
        mb_rewards = np.asarray(mb_rewards, dtype=np.float32)[:endLength]
        mb_actions = np.asarray(mb_actions, dtype=np.float32)[:endLength]
        mb_values = np.asarray(mb_values, dtype=np.float32)
        mb_neglogpacs = np.asarray(mb_neglogpacs, dtype=np.float32)[:endLength]
        mb_dones = np.asarray(mb_dones, dtype=np.bool)
        # discount/bootstrap value function with generalized advantage estimation:
        mb_returns = np.zeros_like(mb_rewards)
        mb_advs = np.zeros_like(mb_rewards)
        for k in range(self.nbPlayer):
            lastgaelam = 0
            for t in reversed(range(k, endLength, self.nbPlayer)):
                nextNonTerminal = 1.0 - mb_dones[t]
                nextValues = mb_values[t + self.nbPlayer]
                delta = mb_rewards[t] + self.gamma * nextValues * nextNonTerminal - mb_values[t]
                mb_advs[t] = lastgaelam = delta + self.gamma * self.lam * nextNonTerminal * lastgaelam

        mb_values = mb_values[:endLength]
        # mb_dones = mb_dones[:endLength]
        mb_returns = mb_advs + mb_values

        return map(sf01, (mb_obs, mb_availAcs, mb_returns, mb_actions, mb_values, mb_neglogpacs))

    def train(self, nTotalSteps):

        nUpdates = nTotalSteps // (self.nGames * self.nSteps)

        for update in range(nUpdates):

            alpha = 1.0 - update / nUpdates
            lrnow = self.learningRate * alpha
            if lrnow < self.minLearningRate:
                lrnow = self.minLearningRate
            cliprangenow = self.clipRange * alpha

            states, availAcs, returns, actions, values, neglogpacs = self.run()

            batchSize = states.shape[0]
            self.totTrainingSteps += batchSize

            nTrainingBatch = batchSize // self.nMiniBatches

            currParams = self.trainingNetwork.getParams()

            mb_lossvals = []
            inds = np.arange(batchSize)
            for _ in range(self.nOptEpochs):
                np.random.shuffle(inds)
                for start in range(0, batchSize, nTrainingBatch):
                    end = start + nTrainingBatch
                    mb_inds = inds[start:end]
                    mb_lossvals.append(self.trainingModel.train(lrnow, cliprangenow, states[mb_inds], availAcs[mb_inds],
                                                                returns[mb_inds], actions[mb_inds], values[mb_inds],
                                                                neglogpacs[mb_inds]))
            lossvals = np.mean(mb_lossvals, axis=0)
            self.losses.append(lossvals)
            losses = np.squeeze(self.losses)
            losses = losses[~np.isnan(losses)].reshape((-1, 3))
            self.minLosses.append(np.argmin(losses[:, 1]))

            newParams = self.trainingNetwork.getParams()
            needToReset = 0
            for param in newParams:
                if np.sum(np.isnan(param)) > 0:
                    needToReset = 1

            if needToReset == 1:
                self.trainingNetwork.loadParams(currParams)

            if update % self.saveEvery == 0 or update == nUpdates - 1:
                name = ("params/model-%06d" % update)
                self.trainingNetwork.saveParams(name)
                joblib.dump(self.losses, "params/losses.pkl")
                joblib.dump(self.epInfos, "params/epInfos.pkl")
                joblib.dump(self.minLosses, "params/minLosses.pkl")

    def competition(self, paramName="params/model-455000", human=1):
        self.reset()
        self.endGame = False
        self.training = False
        params = joblib.load(paramName)
        self.trainingNetwork.loadParams(params)
        while not self.endGame:
            current = self.currentPlayer
            if self.currentPlayer >= human:  # Basic player
                player = self.players[self.currentPlayer]
                action = player.autoPlay()
            else:
                currGos, currStates, currAvailAcs = self.getCurrStates()
                action, values, neglogpacs = self.trainingNetwork.step(currStates, currAvailAcs)
                # nlp = self.trainingModel.neglogp(currStates, currAvailAcs, action)
                # prob = np.exp(-nlp)
                # print("prob: %f" % prob[0])
            # playedCards, ask = gameLogic.decode(action)
            # if not self.training:
            #    print("Step: %d, Player: %d, TopCard: %d, Asked: %d, Action: %d, Ask: %d, PlayedCards: %s, Hand: %s" % (
            #        self.goCounter, current, self.topCard, self.asked, action, ask, playedCards,
            #        self.players[current].hand))
            rewards, dones, infos = self.step(action)
        print("Game Over: Players %s" % (self.players))
        for p in self.players:
            self.players[p].score = gameLogic.SCORE_LIMIT

    def simulate(self, myGame, paramName=None, human=1):
        if paramName:
            params = joblib.load(paramName)
            self.trainingNetwork.loadParams(params)
        self.goCounter += 1
        myGame.init()
        myGame.endGame = False
        rewards = 0
        dones = 0
        while not myGame.endGame:
            current_player = myGame.currentPlayer
            if current_player >= human:  # Basic player
                player = myGame.players[current_player]
                action = player.autoPlay()
            else:
                currGos, currStates, currAvailAcs = myGame.getCurrentState()
                action, values, neglogpacs = self.trainingNetwork.step(currStates, currAvailAcs)
            # print("action = %d" % action)
            rewards, dones, infos = myGame.step(action)
            if myGame.gameOver and not myGame.endGame:
                myGame.reset()
            # print("Game: %d, Player: %d, Action: %d, Done: %d, End Game: %d" % (
            #    self.goCounter, current_player, action, dones, myGame.endGame))

        myGame.players[current_player].fullVictory += 1
        for p in myGame.players:
            myGame.players[p].score = gameLogic.SCORE_LIMIT
            myGame.players[p].totalRewards += myGame.players[p].rewards
        print("Game Over %03d: Done: %d, Rewards: %s, Players: %s" % (self.goCounter, dones, rewards, myGame.players))


if __name__ == "__main__":
    import time
    import sys
    import matplotlib.pyplot as plt

    with tf.Session() as sess:

        nGame = 100
        training = True
        fromParams = None
        if len(sys.argv) > 1:
            training = int(sys.argv[1])
        if len(sys.argv) > 2:
            fromParams = sys.argv[2]
        if len(sys.argv) > 3:
            nGame = int(sys.argv[3])

        if training == 2:
            losses = np.squeeze(joblib.load('params/losses.pkl'))
            epInfos = np.squeeze(joblib.load('params/epInfos.pkl'))
            numTurns = []
            rewards = []
            for epInfo in epInfos:
                numTurns.append(epInfo['numTurns'])
                rewards.append(epInfo['rewards'])
            print(rewards[-50:-1])
            # pg_loss, vf_loss, entropyLoss
            losses = losses[~np.isnan(losses)].reshape((-1, 3))
            pg_loss = losses[:, 0]
            vf_loss = losses[:, 1]
            entropyLoss = losses[:, 2]
            minVFLoss = np.argmin(vf_loss)
            minPGLoss = np.argmin(pg_loss)
            minEnLoss = np.argmin(entropyLoss)
            print("minVFLoss", minVFLoss)
            print("minPGLoss", minPGLoss)
            print("minEnLoss", minEnLoss)
            plt.plot(pg_loss)
            plt.plot(vf_loss)
            plt.plot(entropyLoss)

            plt.xlabel("Epochs")
            plt.ylabel("Value")
            plt.legend(["pg_loss", 'vf_loss', 'entropyLoss', 'minLosses', 'rewards'])
            plt.show()
        elif training == 3:
            losses = np.squeeze(joblib.load('params/losses.pkl'))
            losses = losses[~np.isnan(losses)].reshape((-1, 3))
            vf_loss = losses[:, 1]
            minLosses = np.squeeze(joblib.load('params/minLosses.pkl')).astype(int)
            print(minLosses)
            plt.plot(minLosses, vf_loss[minLosses])
            plt.show()
        else:
            myGame = None
            start = time.time()
            game = gameSimulation(sess, nGames=2, nSteps=20, learningRate=0.00025, clipRange=0.2, saveEvery=500,
                                  fromParams=fromParams)
            if training:
                game.train(100_000)
            else:
                myGame = Game()
                myGame.training = False
                for i in range(nGame):
                    game.simulate(myGame, fromParams, human=1)
            end = time.time()
            if myGame:
                totalRound = myGame.players[0].victory + myGame.players[1].victory
                print("%d vs %d (%.4f), %d vs %d (%.4f)" % (
                    myGame.players[0].victory, myGame.players[1].victory, myGame.players[0].victory / totalRound,
                    myGame.players[0].fullVictory, myGame.players[1].fullVictory,
                    myGame.players[0].fullVictory / nGame))
            # features = np.reshape(game.inputs, (-1, 4 * gameLogic.CARD_LENGTH))
            print("Time Taken: %f" % (end - start))
