import json
import ssl

import gameLogic
import joblib
import numpy as np
import tensorflow as tf
import time
import websocket
from PPONetwork import PPONetwork

GROUP = "GN001"
GROUP_NEW = "GROUP_NEW"
LEAVE = "LEAVE"
REMOVE = "REMOVE"
JOIN = "JOIN"
CAPACITY = 2
PLAYER = "PN001"
SECRET = "SN001"

ASKED = "ASKED"
DRAWN = "DRAWN"
PLAYED = "PLAYED"

PLAY = "PLAY"
ASK = "ASK"

CARD_ENCODING = {"V": gameLogic.JOKER, "D": gameLogic.QUEEN, "R": gameLogic.KING, "7": gameLogic.SEVEN,
                 "9": gameLogic.NINE, "10": gameLogic.TEN, "1": gameLogic.AS, "8": gameLogic.EIGHT,
                 "CARREAU": gameLogic.DIAMOND, "TREFLE": gameLogic.CLUB, "COEUR": gameLogic.HEART,
                 "PIQUE": gameLogic.SPADE
                 }

NUMBERS = {
    gameLogic.JOKER: "V",
    gameLogic.QUEEN: "D",
    gameLogic.KING: "R",
    gameLogic.SEVEN: "7",
    gameLogic.NINE: "9",
    gameLogic.TEN: "10",
    gameLogic.AS: "1",
    gameLogic.EIGHT: "8",
}

COLORS = {
    gameLogic.DIAMOND: "CARREAU",
    gameLogic.CLUB: "TREFLE",
    gameLogic.HEART: "COEUR",
    gameLogic.SPADE: "PIQUE",
}

EN_COLORS = {
    gameLogic.DIAMOND: "D",
    gameLogic.CLUB: "C",
    gameLogic.HEART: "H",
    gameLogic.SPADE: "S",
}

hand = np.array([])
table = np.array([])
topCard = -1
asked = -1

def colorToText(color):
    if color == -1:
        return ""
    return EN_COLORS[color]


def cardToText(card):
    if card == -1:
        return "BC"
    return NUMBERS[gameLogic.cardNumber(card)] + EN_COLORS[gameLogic.cardColor(card)]


def cardsToText(cards):
    return [cardToText(card) for card in cards]


# Tested
def cardsToJson(cards):
    value = []
    for card in cards:
        number = gameLogic.cardNumber(card)
        color = gameLogic.cardColor(card)
        jsonCard = {}
        jsonCard["number"] = NUMBERS[number]
        jsonCard["color"] = COLORS[color]
        value.append(jsonCard)
    return json.dumps(value)


# Tested
def jsonToCard(card):
    number = card["number"]
    color = card["color"]
    return CARD_ENCODING[number] * gameLogic.CARD_COLOR_LENGTH + CARD_ENCODING[color]


# Tested
def jsonToCards(cards):
    value = []
    for card in cards:
        value.append(jsonToCard(card))
    return np.array(value)


class IAPlayer():
    def __init__(self, sess, name=PLAYER, group=GROUP, secret=SECRET, capacity=CAPACITY,
                 endPoint="ws://localhost:8151/ws", cmd="create", fromParams=None):
        self.name = name
        self.group = group
        self.secret = secret
        self.capacity = capacity
        self.endPoint = endPoint
        self.score = gameLogic.SCORE_LIMIT
        self.topCard = -1
        self.asked = -1
        self.hand = np.array([])
        self.table = np.array([])
        self.cmd = cmd
        self.endGame = False
        self.trainingNetwork = PPONetwork(sess, gameLogic.INPUT_DIM, gameLogic.OUTPUT_DIM, "trainNet")
        if fromParams != None:
            params = joblib.load(fromParams)
            self.trainingNetwork.loadParams(params)

        def onMessage(ws, message):
            self.on_message(ws, message)

        def onError(ws, err):
            self.on_error(ws, err)

        def onClose(ws):
            self.on_close(ws)

        def onOpen(ws):
            self.on_open(ws)

        self.ws = websocket.WebSocketApp(endPoint,
                                         on_message=onMessage,
                                         on_error=onError,
                                         on_close=onClose)
        self.ws.on_open = onOpen

    def on_message(self, ws, message):
        data = json.loads(message)
        # print(data)
        if "type" in data:
            type = data["type"]
            if type == "hands":
                self.hand = jsonToCards(data["hands"])
            elif type == "game":
                # print("Game Data received %s" % data)
                if "myGroup" in data:
                    self.state = data["myGroup"]["state"]
                    self.endGame = data["myGroup"]["endGame"]
                    players = data["myGroup"]["players"]
                    for p in players:
                        if p["name"] == self.name:
                            self.score = gameLogic.SCORE_LIMIT - p["point"]
                if "currentTable" in data:
                    self.table = jsonToCards(data["currentTable"])
                if "topCard" in data:
                    card_ = data["topCard"]
                    if card_:
                        self.topCard = jsonToCard(card_)
                    else:
                        self.topCard = -1
                if "color" in data:
                    color = data["color"]
                    if not color:
                        self.asked = -1
                if "cmd" in data:
                    cmd = data["cmd"]
                    self.handleCommand(cmd, ws, data)

    def getState(self):
        currStates = np.zeros(gameLogic.INPUT_DIM)
        if len(self.table) != 0:
            currStates[gameLogic.CARD_LENGTH + self.table] = 1
        if len(self.hand) != 0:
            currStates[self.hand] = 1
        return currStates

    def getCurrState(self):
        currState = self.getState()
        availableActions = gameLogic.getActionVector(self.hand, self.topCard, self.asked)
        availableActions[np.nonzero(availableActions == 1)] = 100
        if availableActions[gameLogic.PASS_INDEX] > 0:  # Do not pass if i can play
            availableActions[gameLogic.PASS_INDEX] = 10
        availableActions[np.nonzero(availableActions == 0)] = -10000
        return currState.reshape(1, gameLogic.INPUT_DIM), availableActions.reshape(1, gameLogic.OUTPUT_DIM)

    def handleCommand(self, cmd, ws, data=None):
        print("Current: Score: %d, TopCard: %s, Asked: %s, Hand: %s, Table: %s" % (
            self.score, cardToText(self.topCard), colorToText(self.asked), cardsToText(self.hand), cardsToText(self.table)))
        if cmd == "PLAY" or cmd == "ASK" and not self.endGame:
            currStates, currAvailAcs = self.getCurrState()
            action, values, neglogpacs = self.trainingNetwork.step(currStates, currAvailAcs)
            playedCards, ask = gameLogic.decode(action)
            print("Playing: Action: %d, Cards: %s, Ask: %s" % (action, cardsToText(playedCards), colorToText(ask)))
            time.sleep(1)
            if ask != -1:
                self.sendAsk(ask, ws)
            elif len(playedCards) == 0:
                print(cmd)
                self.sendDraw(ws)
            else:
                self.sendPlay(playedCards, ws)
        else:
            if cmd == ASKED:
                if "color" in data:
                    color = data["color"]
                    self.asked = CARD_ENCODING[color]
            else:
                print("Unknown command %s or End Game %d" % (cmd, self.endGame))
            if self.endGame:
                self.asked = -1
                self.topCard = -1
                self.table = np.array([])
                self.hand = np.array([])

    def sendAsk(self, ask, ws):
        # ${this.state.playerName}|${ASKED}|${this.state.groupName}|${color}
        ws.send("%s|%s|%s|%s" % (self.name, ASKED, self.group, COLORS[ask]))

    def sendDraw(self, ws):
        # ${this.state.playerName}|${DRAWN}|${this.state.groupName}|${this.state.groupName}
        ws.send("%s|%s|%s|%s" % (self.name, DRAWN, self.group, self.group))

    def sendPlay(self, playedCards, ws):
        # ${this.state.playerName}|${PLAYED}|${this.state.groupName}|${cards}
        ws.send("%s|%s|%s|%s" % (self.name, PLAYED, self.group, cardsToJson(playedCards)))

    def run(self):
        self.ws.run_forever(sslopt={"cert_reqs": ssl.CERT_NONE})

    def on_open(self, ws):
        print("connexion opened")
        if self.cmd == "create":
            self.createGroup()
        if self.cmd == "leave":
            self.leaveGroup()
        if self.cmd == "join":
            self.joinGroup()
        if self.cmd == "remove":
            self.removeGroup()

    def on_error(self, ws, error):
        print(error)

    def on_close(self, ws):
        print("### closed ###")

    def createGroup(self):
        self.ws.send("%s|%s|%s|%s|%s" % (self.group, GROUP_NEW, self.capacity, self.name, self.secret))

    def joinGroup(self):
        self.ws.send("%s|%s|%s|%s" % (self.name, JOIN, self.group, self.secret))

    def removeGroup(self):
        self.ws.send("%s|%s|%s|%s" % (self.group, REMOVE, self.name, self.secret))

    def leaveGroup(self):
        self.ws.send("%s|%s|%s|%s" % (self.name, LEAVE, self.group, self.secret))


if __name__ == "__main__":
    import sys

    #websocket.enableTrace(False)
    with tf.compat.v1.Session() as sess:
        cmd = "create"
        fromParams = "params/model-19000"
        name = "PN001"
        group="GN001"
        capacity=2
        end_point="ws://localhost:8151/ws"
        if len(sys.argv) > 1:
            cmd = sys.argv[1]
        if len(sys.argv) > 2:
            fromParams = sys.argv[2]
        if len(sys.argv) > 3:
            name = sys.argv[3]
        if len(sys.argv) > 4:
            group = sys.argv[4]
        if len(sys.argv) > 5:
            capacity = int(sys.argv[5])
        if len(sys.argv) > 6:
            end_point = sys.argv[6]

        print(cmd)
        player = IAPlayer(sess,endPoint=end_point, group=group, name=name, cmd=cmd, capacity=capacity, fromParams=fromParams)
        player.run()
