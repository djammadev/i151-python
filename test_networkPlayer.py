import networkPlayer
import unittest


class NetworkPlayerTestCase(unittest.TestCase):

    def test_json_to_card(self):
        self.assertEqual(27, networkPlayer.jsonToCard({"number": "1", "color": "PIQUE"}))
        self.assertEqual(31, networkPlayer.jsonToCard({"number": "8", "color": "PIQUE"}))
        self.assertEqual(20, networkPlayer.jsonToCard({"number": "10", "color": "CARREAU"}))
        self.assertEqual(0, networkPlayer.jsonToCard({"number": "V", "color": "CARREAU"}))
        self.assertEqual(4, networkPlayer.jsonToCard({"number": "D", "color": "CARREAU"}))

    def test_cards_to_json(self):
        self.assertEqual('[{"number": "1", "color": "PIQUE"}]', networkPlayer.cardsToJson([27]))
        self.assertEqual('[{"number": "V", "color": "PIQUE"}, {"number": "8", "color": "PIQUE"}]',
                         networkPlayer.cardsToJson([3, 31]))

    def test_json_to_cards(self):
        self.assertEqual([27], networkPlayer.jsonToCards([{"number": "1", "color": "PIQUE"}]))
        self.assertEqual([3, 31], networkPlayer.jsonToCards([{"number": "V", "color": "PIQUE"}, {"number": "8", "color": "PIQUE"}]))


if __name__ == '__main__':
    unittest.main()
