import gameLogic
import numpy as np
import unittest
#import tensorflow as tf

#from GameSimulation import gameSimulation


class GameLogicTestCase(unittest.TestCase):
    def test_get_index_012_cards(self):
        self.assertEqual(22, gameLogic.getIndex(
            np.array([0, 1, 2])))  # 4^(d-1) + (0 * 4^2 + 1 * 4^1 + 2 * 4^0) = 16 + 6 = 22

    def test_get_index_12_cards(self):
        self.assertEqual(10, gameLogic.getIndex(np.array([1, 2])))  # 4^1 + (1 * 4^1 + 2 * 4^0) = 4 + 4 + 2 = 10

    def test_get_index_21_cards(self):
        self.assertEqual(13, gameLogic.getIndex(np.array([2, 1])))  # 4^1 + (2 * 4^1 + 1 * 4^0) = 4 + 8 + 1 = 13

    def test_get_index_3_cards(self):
        self.assertEqual(3, gameLogic.getIndex(np.array([3])))  # (3 * 4^0) = 3

    def test_get_index_cards(self):
        self.assertEqual(gameLogic.PASS_INDEX, gameLogic.getIndex(np.array([])))  # PASS = gameLogic.PASS_INDEX

    def test_get_index_45_cards(self):
        self.assertEqual(79, gameLogic.getIndex(np.array([4, 5])))  # 74 + 4 + 4 * 0 + 1 = 79

    def test_get_index_76_cards(self):
        self.assertEqual(92, gameLogic.getIndex(np.array([7, 6])))  # 74 + 4 + 4 * 3 + 2 = 92

    def test_get_index_28_cards(self):
        self.assertEqual(518, gameLogic.getIndex(np.array([28])))  # 7*74 + 0 = 518

    def test_get_index_4x7_cards(self):
        self.assertEqual(gameLogic.FOUR_EIGHT_INDEX, gameLogic.getIndex(np.array([28, 29, 30, 31])))  # 7*74 + 0 = 518

    def test_get_index_313029_cards(self):
        self.assertEqual(591, gameLogic.getIndex(np.array([31, 30, 29])))  # 7*74 + 4^2 + 3*4^2 + 2*4^1 + 1 = 591

    def test_is_valid(self):
        self.assertEqual(True, gameLogic.isValid(hand=np.array([0]), topCard=-1, asked=-1))
        self.assertEqual(False, gameLogic.isValid(hand=np.array([0, 1]), topCard=-1, asked=-1))
        self.assertEqual(True, gameLogic.isValid(hand=np.array([28, 29]), topCard=-1, asked=-1))
        self.assertEqual(False, gameLogic.isValid(hand=np.array([1]), topCard=6, asked=-1))
        self.assertEqual(True, gameLogic.isValid(hand=np.array([0, 1, 2]), topCard=3, asked=-1))
        self.assertEqual(True, gameLogic.isValid(hand=np.array([0]), topCard=28, asked=0))
        self.assertEqual(False, gameLogic.isValid(hand=np.array([1]), topCard=8, asked=1))
        self.assertEqual(True, gameLogic.isValid(hand=np.array([1]), topCard=31, asked=1))

        # 7 can be played on any card
        for i in range(28):
            for j in range(28, gameLogic.CARD_LENGTH):
                self.assertEqual(True, gameLogic.isValid(hand=np.array([j]), topCard=i, asked=-1))

        # No card can be played on 24-27 except 24-31
        for i in range(24):
            for j in range(24, 28):
                self.assertEqual(False, gameLogic.isValid(hand=np.array([i]), topCard=j, asked=-1))

        # 24-31
        for i in range(24, 28):
            for j in range(24, gameLogic.CARD_LENGTH):
                if i != j:
                    self.assertEqual(True, gameLogic.isValid(hand=np.array([j]), topCard=i, asked=-1))

    def test_eight_actions(self):
        self.assertEqual(0, gameLogic.eightActions(hand=np.array([0, 1, 2, 8, 15])).size / gameLogic.CARD_LENGTH)
        self.assertEqual(1, gameLogic.eightActions(hand=np.array([28])).size / gameLogic.CARD_LENGTH)
        self.assertEqual(3, gameLogic.eightActions(hand=np.array([30, 28, 29])).size / gameLogic.CARD_LENGTH)
        self.assertEqual(4, gameLogic.eightActions(hand=np.array([31, 28, 29, 30])).size / gameLogic.CARD_LENGTH)

    def test_color_actions(self):
        self.assertEqual(2,
                         gameLogic.colorActions(hand=np.array([0, 1, 2, 8, 15]), color=0).size / gameLogic.CARD_LENGTH)
        self.assertEqual(2, gameLogic.colorActions(hand=np.array([0, 1, 2, 8, 15, 28, 31]),
                                                   color=0).size / gameLogic.CARD_LENGTH)

    def test_number_actions(self):
        self.assertEqual(15, gameLogic.numberActions(hand=np.array([0, 1, 2, 8, 15]),
                                                     number=0).size / gameLogic.CARD_LENGTH)
        self.assertEqual(4,
                         gameLogic.numberActions(hand=np.array([0, 1, 8, 15]), number=0).size / gameLogic.CARD_LENGTH)
        self.assertEqual(0, gameLogic.numberActions(hand=np.array([0, 1, 2, 8, 15, 28, 31]),
                                                    number=1).size / gameLogic.CARD_LENGTH)

        self.assertEqual(15, gameLogic.numberActions(hand=np.array([24, 25, 26]),
                                                    number=6).size / gameLogic.CARD_LENGTH)

    def test_available_actions(self):
        self.assertEqual(7, gameLogic.availableActions(
            hand=np.array([0, 1, 2, 4, 7, 28, 30])).size / gameLogic.CARD_LENGTH)  # 15 + 3
        self.assertEqual(18, gameLogic.availableActions(hand=np.array([0, 1, 2, 4, 7, 28, 30]),
                                                        topCard=3).size / gameLogic.CARD_LENGTH)  # 15 + 3

    def test_get_action_vector(self):
        self.assertEqual(7, len(np.nonzero(gameLogic.getActionVector(hand=np.array([0, 1, 2, 4, 7, 28, 30])))[0]))
        actions = np.nonzero(gameLogic.getActionVector(hand=np.array([0, 1, 2, 4, 7, 28]), topCard=29, asked=-1))[0]
        self.assertEqual(4, len(actions))
        actions = np.nonzero(gameLogic.getActionVector(hand=np.array([0, 1, 2, 4, 7, 28]), topCard=29, asked=1))[0]
        self.assertEqual(3, len(actions))
        actions = np.nonzero(gameLogic.getActionVector(hand=np.array([ 3,  4,  8,  9, 19, 20, 23]), topCard=29, asked=2))[0]
        self.assertEqual(1, len(actions))
        actions = np.nonzero(gameLogic.getActionVector(hand=np.array([ 3,  4,  8,  9, 19, 20, 23]), topCard=29, asked=-1))[0]
        self.assertEqual(4, len(actions))

    def test_decode(self):
        hand, ask = gameLogic.decode(gameLogic.ASKED_INDEX)
        self.assertEqual(0, ask)
        self.assertEqual(0, len(hand))
        hand, ask = gameLogic.decode(gameLogic.FOUR_EIGHT_INDEX)
        self.assertEqual(-1, ask)
        self.assertEqual(4, len(hand))
        self.assertEqual(28, hand[0])
        hand, ask = gameLogic.decode(gameLogic.PASS_INDEX)
        self.assertEqual(-1, ask)
        self.assertEqual(0, len(hand))
        hand, ask = gameLogic.decode(gameLogic.getIndex([0, 1, 2]))
        self.assertEqual(-1, ask)
        self.assertEqual(3, len(hand))
        self.assertEqual(0, hand[0])
        self.assertEqual(1, hand[1])
        self.assertEqual(2, hand[2])
        hand, ask = gameLogic.decode(gameLogic.getIndex([3, 1, 2]))
        self.assertEqual(-1, ask)
        self.assertEqual(3, len(hand))
        self.assertEqual(3, hand[0])
        self.assertEqual(1, hand[1])
        self.assertEqual(2, hand[2])
        hand, ask = gameLogic.decode(gameLogic.getIndex([30]))
        self.assertEqual(-1, ask)
        self.assertEqual(1, len(hand))
        self.assertEqual(30, hand[0])
        hand, ask = gameLogic.decode(gameLogic.getIndex([27]))
        self.assertEqual(-1, ask)
        self.assertEqual(1, len(hand))
        self.assertEqual(27, hand[0])
        hand, ask = gameLogic.decode(gameLogic.getIndex([28, 29, 30, 31]))
        self.assertEqual(-1, ask)
        self.assertEqual(4, len(hand))
        self.assertEqual(28, hand[0])
        self.assertEqual(31, hand[3])

    def test_rewards(self):
        self.assertEqual(10, gameLogic.getRewards([gameLogic.TEN * 4]))
        self.assertEqual(32, gameLogic.getRewards([gameLogic.EIGHT * 4]))
        self.assertEqual(11, gameLogic.getRewards([gameLogic.AS * 4 + 3]))
        self.assertEqual(21, gameLogic.getRewards([gameLogic.AS * 4 + 3, gameLogic.TEN * 4]))

    def test_available_action(self):
        actions = gameLogic.getActionVector(hand=np.array([0, 1, 2, 4, 7, 28, 30]), topCard=3)
        for action in np.nonzero(actions)[0]:
            played, ask = gameLogic.decode(action)
            if len(played) > 0:
                self.assertEqual(True, gameLogic.isValid(played, topCard=3, asked=-1))
        actions = gameLogic.getActionVector(hand=np.array([4, 7, 28, 30]), topCard=3)
        for action in np.nonzero(actions)[0]:
            played, ask = gameLogic.decode(action)
            if len(played) > 0:
                self.assertEqual(True, gameLogic.isValid(played, topCard=3, asked=-1))
        actions = gameLogic.getActionVector(hand=np.array([4, 7, 28, 30]), topCard=29, asked=1)
        for action in np.nonzero(actions)[0]:
            played, ask = gameLogic.decode(action)
            if len(played) > 0:
                self.assertEqual(True, gameLogic.isValid(played, topCard=29, asked=1))

    def test_player_available_actions(self):
        hand=np.array([1, 3, 5, 7, 8, 11, 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 21])
        actions = gameLogic.availableActions(hand, topCard=16)
        actions = np.reshape(actions, (-1, gameLogic.CARD_LENGTH))
        A = gameLogic.convertAvailableActions(actions)
        X = np.zeros(gameLogic.CARD_LENGTH)
        X[hand] = 1
        S = gameLogic.getScore(X - A, gameLogic.SCORE_LIMIT)
        index = np.argmax(S)
        played = actions[index]
        result = np.array([])
        for i in range(1, 5):
            result = np.append(result, np.nonzero(played == i))
        self.assertEqual(len(S) - 1, index)

    def test_3_as_actions(self):
        hand=np.array([24,25,26])
        actions = gameLogic.getActionVector(hand, topCard=27)
        self.assertEqual(16, len(np.nonzero(actions)[0]))
        index = gameLogic.getIndex(hand)
        self.assertEqual(1, actions[index])

    def test_encode_obs(self):
        table = np.array([1, 0, 0, 1])
        value = gameLogic.encodeObs(table)
        self.assertEqual(9, value)

        table = np.array([0, 0, 0, 0])
        value = gameLogic.encodeObs(table)
        self.assertEqual(0, value)

        table = np.array([1, 1, 1, 1])
        value = gameLogic.encodeObs(table)
        self.assertEqual(15, value)

    def test_enumerate_all(self):
        actions = []
        for action in range(gameLogic.OUTPUT_DIM):
            hand, ask = gameLogic.decode(action)
            if gameLogic.isProbableHand(hand):
                actions.append(str(action))
                print("action = %d, hand = %s, ask = %d" % (action, hand, ask))
        print(','.join(actions))



#    def test_player_play(self):
#        with tf.Session() as sess:
#            game = gameSimulation(sess, nGames=1, nSteps=20, learningRate=0.00025, clipRange=0.2)
#            player = gameLogic.Player(game, None, "Player")
#            hand=np.array([1, 3, 5, 7, 8, 11, 12, 13, 14, 15, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 21])
#            player.hand = hand
#            game.topCard = 16
#            action = player.autoPlay()
#            self.assertEqual(gameLogic.FOUR_EIGHT_INDEX, action)



if __name__ == '__main__':
    unittest.main()
